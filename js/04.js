// 今日の日付を取得する
// Dateオブジェクトのインスタンスを作り、todayという変数に代入
let today = new Date();

// コンソールに出力
console.log(today);

// 日付や曜日を個別に取得してみる。
// 日付オブジェクトのメソッドを使用する
console.log('年：' + today.getFullYear());

// 月は0～11の値が戻ってくるので、1足してあげる。
console.log('月：');

console.log('日：' + today.getDate());

// 曜日は0～6が戻ってくる。日曜日～土曜日を意味するので変換する
// 値に応じて条件分岐させる。（if文にしているが、この場合は、switch文を使ったほうがスマート）

console.log('曜日：');