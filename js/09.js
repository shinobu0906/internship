/**
 * 最小値（min）から最大値（max）の間で、ランダムに数値を生成し合算する。
 * 
 * @param {string} idName - 数値を表示する領域のid
 * @param {number} min - 乱数の最小値
 * @param {number} max - 乱数の最大値
 * @param {number} interval - 乱数を発生させる間隔
 * @param {number} times - 乱数を発生させる回数
 */
function execute(idName, min, max, interval, times) {
    const number = document.querySelector('#' + idName);
    let increment = 0;
    let total = 0;

    number.innerText = '';

    // 指定された間隔で乱数発生
    const intervalID = setInterval(() => {
        increment++;

        // 偶数回目の繰り返しの場合、表示領域をクリアする
        if (increment % 2) {
            number.innerText = "";
        } else {
            let tmp = Math.floor(Math.random() * (max + 1 - min)) + min;    // 乱数生成
            total = total + tmp;
            number.innerText = tmp
        }
        
    }, interval);

    // 指定回数 乱数発生が終わった場合の処理
    setTimeout(() => {
        clearInterval(intervalID);
        console.log('処理終了');
        setTimeout(() => {
            alert("合計は？");
            number.innerText = total;
        }, interval);
    }, interval * times * 2);
}
