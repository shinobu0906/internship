// 今日の日付を取得する
// Dateオブジェクトのインスタンスを作り、todayという変数に代入
let today = new Date();

// コンソールに出力
console.log(today);

// 日付や曜日を個別に取得してみる。
// 日付オブジェクトのメソッドを使用する
console.log('年：' + today.getFullYear());
console.log('月：' + today.getMonth());
console.log('日：' + today.getDate());
console.log('曜日：' + today.getDay());