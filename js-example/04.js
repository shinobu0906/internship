// 今日の日付を取得する
// Dateオブジェクトのインスタンスを作り、todayという変数に代入
let today = new Date();

// コンソールに出力
console.log(today);

// 日付や曜日を個別に取得してみる。
// 日付オブジェクトのメソッドを使用する
console.log('年：' + today.getFullYear());

// 月は0～11の値が戻ってくるので、1足してあげる。括弧で囲まないとうまく足し算されないので注意
console.log('月：' + (today.getMonth()+1));
console.log('日：' + today.getDate());

// 曜日は0～6が戻ってくる。日曜日～土曜日を意味するので変換する
// 値に応じて条件分岐させる。（if文にしているが、この場合は、switch文を使ったほうがスマート）
let dayOfWeek = today.getDay();
let output;
if (dayOfWeek == 0) {
    output = "日曜日";
} else if (dayOfWeek == 1) {
    output = "月曜日";
} else if (dayOfWeek == 2) {
    output = "火曜日";
} else if (dayOfWeek == 3) {
    output = "水曜日";
} else if (dayOfWeek == 4) {
    output = "木曜日";
} else if (dayOfWeek == 5) {
    output = "金曜日";
} else if (dayOfWeek == 6) {
    output = "土曜日";
}

console.log('曜日：' + output);